#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

#define PORT 8080
#define MAX_LENGTH 1024

void receive_file(int socket, char *fname);

int main(int argc, char const *argv[]) {
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[1024] = {0};
      
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
      
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );
      
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
        perror("accept");
        exit(EXIT_FAILURE);
    }

    receive_file(new_socket, "hartakarun.zip");
    return 0;
}

void receive_file(int socket, char *fname)
{
    char buffer[MAX_LENGTH] = {0};
    char fpath[MAX_LENGTH];
    
    strcpy(fpath, "./soal3/Server/");
    strcat(fpath, fname);
    FILE *file_masuk = fopen(fpath, "wb");

    if (file_masuk == NULL) printf("File %s, can't be made.\n", fname);
    else
    {
        memset(buffer, 0, MAX_LENGTH);
        int file_size = 0;
        
        while ((file_size = recv(socket, buffer, MAX_LENGTH, 0)) > 0)
        {
            int write_size = fwrite(buffer, sizeof(char), file_size, file_masuk);
            
            if (write_size < file_size){
                perror("Failed to write file.");
                return;
            }
            memset(buffer, 0, MAX_LENGTH);
            if (file_size == 0 || file_size != MAX_LENGTH) break;
        }
        
        if (file_size < 0)
        {
            fprintf(stderr, "Downloading file failed\n");
            exit(1);
        }
    }
  fclose(file_masuk);
  printf("Terima File Berhasil\n");
}