#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <pthread.h>
#include <dirent.h>
#include <stdlib.h>
#include <ctype.h>


char* getExt(char *namaFile);
void tranversalDirectory(char *basePath);
void createDirectory(char *dir_Name);
void moveFile(char *namaFile, char *origin, char *destination);
void *processFile(void *argv);
void directoryTranverse(char *basePath);
void joinThread();

pthread_t thread[100];
pthread_mutex_t process_mutex;
int it=-1;

int main()
{
    pthread_mutex_init(&process_mutex, NULL);

    chdir("hartakarun");
    tranversalDirectory(".");
    joinThread();

    pthread_mutex_destroy(&process_mutex);

    return 0;
}

void joinThread(){
    for(; it>=0; it--){
        pthread_join( thread[it], NULL);
    }
}

void *processFile(void *argv){
    pthread_mutex_lock(&process_mutex);
    char **temp = (char**) argv;
    char *ext = getExt(temp[0]);

    createDirectory(ext);
    moveFile(temp[0], temp[1], ext);

    free(argv);
    pthread_mutex_unlock(&process_mutex);
}

void moveFile(char *namaFile, char *origin, char *destination){
    char dest[2000];
    
    snprintf(dest, sizeof(dest), "./%s/%s", destination, namaFile);
    printf("%d %s\n", rename(origin, dest), origin);
}

void createDirectory(char *dir_Name){
    mkdir(dir_Name,0777);
}

char* getExt(char *namaFile){
    char *ext;
    int len = sizeof(namaFile) * sizeof(char*) + 1000;
    char str[len];

    strcpy(str, namaFile);

    if(str[0] == '.' && strcmp(str, ".") != 0 && strcmp(str, "..") != 0) return "hidden";

    ext = strtok(str, ".");
    ext = strtok(NULL, "");

    if(ext == NULL) return "unknown";

    int i=0;
    while(ext[i]){
       ext[i] = tolower(ext[i]);
       i++;
    }

    return ext;
}

void tranversalDirectory(char *basePath)
{
    char path[1000];
    struct dirent *dp;
    DIR *dir = opendir(basePath);
    DIR* check;
    if (!dir) return;

    while ((dp = readdir(dir)) != NULL)
    {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
        {
            strcpy(path, basePath);
            strcat(path, "/");
            strcat(path, dp->d_name);

            check = opendir(path);
            if(!check){
                char** argv = (char**)malloc(2 * sizeof(char*));
                argv[0] = (char*)malloc(2000 * sizeof(char));
                argv[1] = (char*)malloc(1000 * sizeof(char));

                strcpy(argv[0], dp->d_name);
                strcpy(argv[1], path);
                
                pthread_create(&(thread[++it]), NULL, processFile, argv);
            }
            tranversalDirectory(path);
        }
    }
    closedir(dir);
}