#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>

#define PORT 8080
#define MAX_LENGTH 1024


void send_file(int socket, char *fname);

int main(int argc, char const *argv[]) {

    struct sockaddr_in address;
    int sock = 0, valread;
    struct sockaddr_in serv_addr;
    char buffer[MAX_LENGTH] = {0};
    int status;

    if(fork() == 0){
        char *argv[] = {"zip", "-r", "./soal3/Client/hartakarun.zip", "-j", "/home/ubuntu/shift3/hartakarun/", NULL};
        execv("/bin/zip", argv);
    }
    while ((wait(&status)) > 0);

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }
  
    memset(&serv_addr, '0', sizeof(serv_addr));
  
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
      
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
  
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }
    char* perintah, namafile;
    scanf("%s %s", perintah, namafile);

    if(strcmp(perintah, "send") == 0) send_file(sock, namafile);
    else printf("The command is only 'send'");
    return 0;
}

void send_file(int socket, char *fname)
{
    char buffer[MAX_LENGTH] = {0};
    char fpath[MAX_LENGTH];

    strcpy(fpath, "./soal3/Client/");
    strcat(fpath, fname);
    FILE *file = fopen(fpath, "r");
    
    if (file == NULL)
    {
        printf("File %s not found.\n", fname);
        return;
    }

    memset(buffer, 0, MAX_LENGTH);

    int file_size;

    while (file_size > 0)
    {   
        file_size = fread(buffer, sizeof(char), MAX_LENGTH, file);
        if (send(socket, buffer, file_size, 0) < 0)
        {
            fprintf(stderr, "Failed to send file %s\n", fname);
            return;
        }
        memset(buffer, 0, MAX_LENGTH);
    }
    fclose(file);
    printf("Kirim File Berhasil\n");
}