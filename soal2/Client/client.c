#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#define PORT 8080

char input[1024];
int sock = 0, valread;
struct sockaddr_in address;
struct sockaddr_in serv_addr;
char cmd[1024], clientIn[1024];
char dirPath[100] = "/home/ubuntu/Desktop/sisop/modul3/soal-shift-sisop-modul-3-ita04-2022/soal2/Client";
int isValidc = 0;
char judul[1024];

void changeInput(char in[1024]){
    strcpy(input,"");
    strcat(input,in);
}
void readBuffer(){
    char buffer[1024] = {0};
    valread = read( sock , buffer, 1024);
    strcpy(cmd,buffer);
    printf("Server: %s\n",cmd );
}
void sendInput(){
    send(sock , input , strlen(input) , 0 );
}
void readBufferSp(){
    char buffer[1024] = {0};
    valread = read( sock , buffer, 1024);
    strcpy(cmd,buffer);
}
void sendFile(){
    strcpy(input,"");
    FILE * fp;
    fp = fopen (clientIn, "r");
    int ct=0;
    do
    {
        char c = fgetc(fp);
        if (feof(fp))
            break ;
 
        input[ct]=c;
        ct++;
    }  while(1);
    input[ct]='\0';
    fclose(fp); 
    send(sock , input , strlen(input) , 0 );
}

int main(int argc, char const *argv[]) {
    strcpy(cmd,"");
    
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }
  
    memset(&serv_addr, '0', sizeof(serv_addr));
  
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
      
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
  
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }

    strcpy(cmd, "");
    printf("Connected to Server\n");
    readBufferSp();
    printf("Server: %s\n",cmd );
    while (strcmp(cmd, "Register / Login? (case sensitive)")!=0) {
        printf("Refresh connection with random chat\n");
        scanf("%[^\n]%*c", clientIn);
        changeInput(clientIn);
        sendInput();
        readBufferSp();
        printf("Server: %s\n",cmd );
    }
    
    strcpy(cmd, "");
    scanf("%[^\n]%*c", clientIn);
    changeInput(clientIn);
    sendInput();
    readBufferSp();
    printf("Server: %s\n",cmd );
    while(strcmp(cmd,"Register / Login?")==0){
        scanf("%[^\n]%*c", clientIn);
        changeInput(clientIn);
        sendInput();
        readBufferSp();
        printf("Server: %s\n",cmd );
    }
    
    if(strcmp(cmd,"Silakan Login")==0 || strcmp(cmd,"Silakan Login\nServer: Masukkan username")==0){
        isValidc = 0;
        while(!isValidc){
            // input username
            scanf("%[^\n]%*c", clientIn);
            changeInput(clientIn);
            sendInput();

            // input password
            readBuffer();
            scanf("%[^\n]%*c", clientIn);
            changeInput(clientIn);
            sendInput();
            
            readBufferSp();
            printf("Server: %s\n",cmd );
            if(strcmp(cmd, "Login berhasil")==0)isValidc=1;
        }
    }
    else if(strcmp(cmd,"Silakan Register")==0 || strcmp(cmd,"Silakan Register\nServer: Masukkan username")==0){
        isValidc = 0;
        while(!isValidc){
            // input username
            scanf("%[^\n]%*c", clientIn);
            changeInput(clientIn);
            sendInput();

            // input password
            readBuffer();
            scanf("%[^\n]%*c", clientIn);
            changeInput(clientIn);
            sendInput();
            
            readBufferSp();
            printf("Server: %s\n",cmd );
            if(strcmp(cmd, "Register berhasil")==0)isValidc=1;
        }
    }
    while(1){
    scanf("%[^\n]%*c", clientIn);
    changeInput(clientIn);
    sendInput();
        if(strcmp(clientIn,"see")==0){
            readBufferSp();
            printf(".tsv list:\n%s",cmd);
        }
        else if(strcmp(clientIn,"logout")==0){
            readBuffer();
            break;
        }
        else if(strcmp(clientIn,"add")==0){
            //masukkan judul
            readBuffer();
            scanf("%[^\n]%*c", clientIn);
            changeInput(clientIn);
            sendInput();
            readBuffer();
            while(strcmp(cmd,"Directory exists, ganti judul problem:")==0){
                scanf("%[^\n]%*c", clientIn);
                changeInput(clientIn);
                sendInput();
                readBufferSp();
                printf("Server: %s\n",cmd);
            }
            readBuffer();
        }
        else {
            if(strcmp(clientIn,"download")==0){
                readBuffer();
                scanf("%[^\n]%*c", clientIn);
                changeInput(clientIn);
                sendInput();
                readBuffer();
                while(strcmp(cmd,"Directory doesn't exist, ganti judul problem:")==0){
                    scanf("%[^\n]%*c", clientIn);
                    changeInput(clientIn);
                    sendInput();
                    readBufferSp();
                    printf("Server: %s\n",cmd);
                }
                readBuffer();
            }
            else if(strcmp(clientIn,"submit")==0){
                readBuffer();
                scanf("%[^\n]%*c", clientIn);
                changeInput(clientIn);
                sendInput();
                readBuffer();//AC or WA
            }
            else readBuffer();
        }
    }
    return 0;
}
