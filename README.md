# soal-shift-sisop-modul-3-ITA04-2022

# Soal No. 1

## Cara Pengerjaan

## SOAL
1. Novak adalah seorang mahasiswa biasa yang terlalu sering berselancar di internet. Pada 
   suatu saat, Ia menemukan sebuah informasi bahwa ada suatu situs yang tidak memiliki 
   pengguna. Ia mendownload berbagai informasi yang ada dari situs tersebut dan menemukan
   sebuah file dengan tulisan yang tidak jelas. Setelah diperhatikan lagi, kode tersebut
   berformat base64. Ia lalu meminta kepada anda untuk membuat program untuk
   memecahkan kode-kode di dalam file yang Ia simpan di drive dengan cara decoding dengan
   base 64. Agar lebih cepat, Ia sarankan untuk menggunakan thread.
	 a. Download dua file zip dan unzip file zip tersebut di dua folder yang berbeda dengan
          nama quote untuk file zip quote.zip dan music untuk file zip music.zip. Unzip ini
          dilakukan dengan bersamaan menggunakan thread.
	 b. Decode semua file .txt yang ada dengan base 64 dan masukkan hasilnya dalam satu
          file .txt yang baru untuk masing-masing folder (Hasilnya nanti ada dua file .txt) pada
          saat yang sama dengan menggunakan thread dan dengan nama quote.txt dan
          music.txt. Masing-masing kalimat dipisahkan dengan newline/enter.
	 c. Pindahkan kedua file .txt yang berisi hasil decoding ke folder yang baru bernama
          hasil.
	 d. Folder hasil di-zip menjadi file hasil.zip dengan password 'mihinomenest[Nama
          user]'. (contoh password : mihinomenestnovak)
 	 e. Karena ada yang kurang, kalian diminta untuk unzip file hasil.zip dan buat file no.txt
          dengan tulisan 'No' pada saat yang bersamaan, lalu zip kedua file hasil dan file no.txt
          menjadi hasil.zip, dengan password yang sama seperti sebelumnya.


##Cara Pengerjaan##
pertama-tama membuat program decode supaya file dapat disandikan

pthread_t thread[2];
pthread_mutex_t process_mutex;
int it=-1;
static char *decoding_table = NULL;
FILE *fp, *fptr;
char buffer[4096];
static char encoding_table[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                                'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                                'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                                'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                                'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                                'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                                'w', 'x', 'y', 'z', '0', '1', '2', '3',
                                '4', '5', '6', '7', '8', '9', '+', '/'};

Setelah itu membuat directory dengan nama music dan quote, lalu ekstrak
file music.zip ke dalam folder music, dan file quote.zip ke dalam folder
quote

int main()
{
    char *argv1, *argv2;
    pthread_mutex_init(&process_mutex, NULL);

    argv1 = "./soal1/music"; argv2 = "./soal1/quote";
    pthread_create(&(thread[++it]), NULL, createDirectory, argv1);
    pthread_create(&(thread[++it]), NULL, createDirectory, argv2);
    joinThread();

    char *arrgv1[] = {"music.zip", "./soal1/music"};
    char *arrgv2[] = {"quote.zip", "./soal1/quote"};
    pthread_create(&(thread[++it]), NULL, ekstrak, arrgv1);
    pthread_create(&(thread[++it]), NULL, ekstrak, arrgv2);
    joinThread();

    char *arrgv3[] = {"./soal1/quote", "./soal1/quote/quote.txt"};
    char *arrgv4[] = {"./soal1/music", "./soal1/music/music.txt"};
    pthread_create(&(thread[++it]), NULL, processFile, arrgv3);
    pthread_create(&(thread[++it]), NULL, processFile, arrgv4);
    joinThread();


File yang terekstrak akan menjadi file bernama music.txt dan quote.txt
Kemudian file music.txt dan quote.txt didecode dan hasilnya dimasukkan ke
dalam folder hasil

*Decode

void tranversalDirectory(char *basePath)
{
    char path[1000];
    struct dirent *dp;
    DIR *dir = opendir(basePath);
    DIR* check;
    if (!dir) return;

    while ((dp = readdir(dir)) != NULL)
    {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
        {
            strcpy(path, basePath);
            strcat(path, "/");
            strcat(path, dp->d_name);

            check = opendir(path);
            if(!check){
                fp = fopen(path, "r");
                fread(buffer, 4096, 1, fp);

                fputs(base64_decode(buffer), fptr);
                fputs("\n", fptr);
                
                fclose(fp);
            }
            tranversalDirectory(path);
        }
    }
    closedir(dir);
}

unsigned char *base64_decode(const char *data) {

    size_t input_length = strlen(data);
    size_t *output_length;

    if (decoding_table == NULL) build_decoding_table();

    if (input_length % 4 != 0) return NULL;

    *output_length = input_length / 4 * 3;
    if (data[input_length - 1] == '=') (*output_length)--;
    if (data[input_length - 2] == '=') (*output_length)--;

    unsigned char *decoded_data = malloc(*output_length);
    if (decoded_data == NULL) return NULL;

    for (int i = 0, j = 0; i < input_length;) {

        uint32_t sextet_a = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_b = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_c = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_d = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];

        uint32_t triple = (sextet_a << 3 * 6)
        + (sextet_b << 2 * 6)
        + (sextet_c << 1 * 6)
        + (sextet_d << 0 * 6);

        if (j < *output_length) decoded_data[j++] = (triple >> 2 * 8) & 0xFF;
        if (j < *output_length) decoded_data[j++] = (triple >> 1 * 8) & 0xFF;
        if (j < *output_length) decoded_data[j++] = (triple >> 0 * 8) & 0xFF;
    }

    return decoded_data;
}

void build_decoding_table() {

    decoding_table = malloc(256);

    for (int i = 0; i < 64; i++)
        decoding_table[(unsigned char) encoding_table[i]] = i;
}

void* processFile(void *argv){
    pthread_mutex_lock(&process_mutex);

    char **file = (char**) argv;

    fptr = fopen(file[1], "a");
    tranversalDirectory(file[0]);
    fclose(fptr);
    pthread_mutex_unlock(&process_mutex);
}

void* ekstrak(void *argv){
    pthread_mutex_lock(&process_mutex);

    char **data = (char**) argv;
    
    char *arg[] = {"unzip", "-j", data[0], "-d", data[1], NULL};
    jalankan("/bin/unzip", arg);

    pthread_mutex_unlock(&process_mutex);
}

void* createDirectory(void *argv){
    pthread_mutex_lock(&process_mutex);

    char* dir = (char*) argv;
    
    char *arg[] = {"mkdir", "-p", dir, NULL};
    jalankan("/bin/mkdir", arg);

    pthread_mutex_unlock(&process_mutex);
}

void jalankan(char *perintah, char*argv[]){
    int status=0;
    if(fork()==0)
        execv(perintah, argv);
    while(wait(&status)>0);
}

void joinThread(){
    for(; it>=0; it--){
        pthread_join( thread[it], NULL);
    }
}

*Memasukkan ke dalam folder hasil

char *argv3 = "./soal1/hasil";
    createDirectory(argv3);

    char *arrgv5[] = {"mv", "./soal1/quote/quote.txt", "./soal1/hasil/quote.txt", NULL};
    char *arrgv6[] = {"mv", "./soal1/music/music.txt", "./soal1/hasil/music.txt", NULL};
    jalankan("/usr/bin/mv", arrgv5);
    jalankan("/usr/bin/mv", arrgv6);

Setelah itu folder hasil di zip dan di password

char *arrgv7[] = {"zip", "--password", "mihinomenestubuntu", "-r", "./soal1/hasil.zip", "-j", "./soal1/hasil", NULL};
    jalankan("/bin/zip", arrgv7);

    pthread_create(&(thread[++it]), NULL, permintaanKhusus1, NULL);
    pthread_create(&(thread[++it]), NULL, permintaanKhusus2, NULL);

    char *arrgv8[] = {"zip", "--password", "mihinomenestubuntu", "-r", "./soal1/hasil.zip", "-j", "./soal1/hasil", NULL};
    jalankan("/bin/zip", arrgv8);

    pthread_mutex_destroy(&process_mutex);
    return 0;
}

Kemudian file di unzip lagi dan membuat file no.txt, kemudian hasil decode
dan file no.txt di zip kembali dengan password yang sama

void* permintaanKhusus1(){
    char *argv7[] = {"unzip", "--password", "mihinomenestubuntu", "-r", "./soal1/hasil.zip", "-j", "./soal1/hasil", NULL};
    jalankan("/bin/unzip", argv7);
}

void* permintaanKhusus2(){
    FILE *file;
    file = fopen("./soal1/hasil/no.txt", "w");
    fputs("No", file);
    fclose(file);
}

## Screenshot

![gambar 1](https://gitlab.com/fernandosibarani45/screenshot/-/raw/master/soal-shift-sisop-modul-3-ita04-2022/soal%201/messageImage_1650208406287.jpg)
![gambar 2](https://gitlab.com/fernandosibarani45/screenshot/-/raw/master/soal-shift-sisop-modul-3-ita04-2022/soal%201/messageImage_1650208526297.jpg)
![gambar 3](https://gitlab.com/fernandosibarani45/screenshot/-/raw/master/soal-shift-sisop-modul-3-ita04-2022/soal%201/messageImage_1650208618911.jpg)
![gambar 4](https://gitlab.com/fernandosibarani45/screenshot/-/raw/master/soal-shift-sisop-modul-3-ita04-2022/soal%201/messageImage_1650208712032.jpg)

## Kendala yang dihadapi##
Hasil tidak sepenuhnya benar dalam decode kode

# Soal No. 2
Pada soal ini, program akan dibagi menjadi `server.c` dan `client.c` yang masing-masing berada dalam directory `Server` dan `Client`.
Program diharapkan dapat menjalankan fungsi seperti online judge berbasis client-server. Dalam menyelesaikan soal ini, terdapat beberapa fungsi utilitas yang digunakan
pada client-side (juga digunakan pada server-side dengan sock diganti new_socket):
```c
void changeInput(char in[1024]){
    strcpy(input,"");
    strcat(input,in);
}
void readBuffer(){
    char buffer[1024] = {0};
    valread = read( sock , buffer, 1024);
    strcpy(cmd,buffer);
    printf("Server: %s\n",cmd );
}
void sendInput(){
    send(sock , input , strlen(input) , 0 );
}
void readBufferSp(){
    char buffer[1024] = {0};
    valread = read( sock , buffer, 1024);
    strcpy(cmd,buffer);
}
```
`sendInput()` berfungsi mengirim string input antar server-client, `readBuffer()` berfungsi menerima input dari lawan lalu print dengan format `<pengirim>: <isi pesan>`,
`changeInput()` berfungsi mengubah isi array input yang akan digunakan untuk mengirim pesan, `readBufferSp()` mirip seperti `readBuffer()` tetapi menyimpan hasil read
pada array cmd yang selanjutnya digunakan pada kondisional.

## 2A.
## Server-side
```c
    int new_socket = *(int *)tmp;
	char out[100];
    printf("A client connected\n");
    if (ctrClient == 1) {
	changeInput("Register / Login? (case sensitive)");
	sendInput(new_socket);
    }
    else {
	changeInput("Wait other client to close connection...");
	sendInput(new_socket);
    }
    
    while (ctrClient > 1) {
        readBuffer(new_socket);
        if (ctrClient == 1) {
            changeInput("Register / Login? (case sensitive)");
	    sendInput(new_socket);
        }
        else {
            changeInput("Wait other client to close connection...");
	    sendInput(new_socket);
        }
    }

    readBufferSp(new_socket);
    while(strcmp(cmd,"Login")!=0 && strcmp(cmd,"Register")!=0){
        changeInput("Register / Login? (case sensitive)");
        sendInput(new_socket);
        readBufferSp(new_socket);
    }
    
    strcpy(socketIn,"Silakan ");
    strcat(socketIn,cmd);
    strcat(socketIn,"\nServer: ");
	
    if(strcmp(cmd,"Login")==0){
		isValid=0, isValidPass=0;
		while(!isValid || !isValidPass){
			strcat(socketIn,"Masukkan username");
			changeInput(socketIn);
			sendInput(new_socket);
			readBufferSp(new_socket);
			strcpy(usernameArr,cmd);

			changeInput("Masukkan password");
			sendInput(new_socket);
			readBufferSp(new_socket);
			strcpy(passwordArr,cmd);

			findUser();
			if(isValid && isValidPass)break; // username ditemukan dan password sesuai
			strcpy(socketIn,"Username atau password salah, ulangi proses !\nServer: ");
		}
		changeInput("Login berhasil");
		sendInput(new_socket);
	}
	else if(strcmp(cmd,"Register")==0){
		isValid=1, isValidRe=0;
		while(isValid || !isValidRe){
			strcat(socketIn,"Masukkan username");
			changeInput(socketIn);
			sendInput(new_socket);
			readBufferSp(new_socket);
			strcpy(usernameArr,cmd);

			changeInput("Masukkan password (ada angka, lowercase, uppercase, 6 huruf)");
			sendInput(new_socket);
			readBufferSp(new_socket);
			strcpy(passwordArr,cmd);
			
				findUser();
			if(!isValid && isValidRe)break; // username belum ditemukan dan kriteria password sesuai
			strcpy(socketIn,"Data tidak valid atau sudah ada, ulangi proses !\nServer: ");
			isValid=1, isValidRe=0;	
		}
		changeInput("Register berhasil");
		sendInput(new_socket);
		addUser();
```
### Client-side
```c
    strcpy(cmd, "");
    printf("Connected to Server\n");
    readBufferSp();
    printf("Server: %s\n",cmd );
    while (strcmp(cmd, "Register / Login? (case sensitive)")!=0) {
        printf("Refresh connection with random chat\n");
        scanf("%[^\n]%*c", clientIn);
        changeInput(clientIn);
        sendInput();
        readBufferSp();
        printf("Server: %s\n",cmd );
    }
    
    strcpy(cmd, "");
    scanf("%[^\n]%*c", clientIn);
    changeInput(clientIn);
    sendInput();
    readBufferSp();
    printf("Server: %s\n",cmd );
    while(strcmp(cmd,"Register / Login?")==0){
        scanf("%[^\n]%*c", clientIn);
        changeInput(clientIn);
        sendInput();
        readBufferSp();
        printf("Server: %s\n",cmd );
    }
    
    if(strcmp(cmd,"Silakan Login")==0 || strcmp(cmd,"Silakan Login\nServer: Masukkan username")==0){
        isValidc = 0;
        while(!isValidc){
            // input username
            scanf("%[^\n]%*c", clientIn);
            changeInput(clientIn);
            sendInput();

            // input password
            readBuffer();
            scanf("%[^\n]%*c", clientIn);
            changeInput(clientIn);
            sendInput();
            
            readBufferSp();
            printf("Server: %s\n",cmd );
            if(strcmp(cmd, "Login berhasil")==0)isValidc=1;
        }
    }
    else if(strcmp(cmd,"Silakan Register")==0 || strcmp(cmd,"Silakan Register\nServer: Masukkan username")==0){
        isValidc = 0;
        while(!isValidc){
            // input username
            scanf("%[^\n]%*c", clientIn);
            changeInput(clientIn);
            sendInput();

            // input password
            readBuffer();
            scanf("%[^\n]%*c", clientIn);
            changeInput(clientIn);
            sendInput();
            
            readBufferSp();
            printf("Server: %s\n",cmd );
            if(strcmp(cmd, "Register berhasil")==0)isValidc=1;
        }
    }
```
### createTxt() dan findUser()
```c
void createTxt(){
    strcpy(txtPath,dirPath);
    strcat(txtPath,"/users.txt");
    FILE * fp;
    fp = fopen (txtPath, "a+");
    fclose(fp);   
}
void findUser(){
    isValid=0, isValidPass=0, isValidRe=0;
    FILE * fp;
    fp = fopen (txtPath, "r");
    checkEmpty=0;
    while (fscanf(fp,"%s\n",fromFile) != EOF){
	    checkEmpty++;
	    int i=0, j=0;
	    char userTemp[100], passTemp[100];
	    strcpy(userTemp,"");strcpy(passTemp,"");
	    while(fromFile[i] && fromFile[i]!=':'){
	        userTemp[j]=fromFile[i];
		i++;j++;
	    }
	    userTemp[j]='\0';
	    i++;j=0;
	    while(fromFile[i] && fromFile[i]!='\n'){
	        passTemp[j]=fromFile[i];
		i++;j++;
	    }
	    passTemp[j]='\0';
	    
	    if(strcmp(userTemp, usernameArr)==0)isValid = 1; // username ditemukan
	    if(strcmp(passTemp, passwordArr)==0)isValidPass = 1; // password benar
	    int cekzz[4]={0};
	    for(int i=0;i<strlen(passwordArr);i++){
	        if(passwordArr[i]>=48 && passwordArr[i]<=57)cekzz[0]=1;
	        if(passwordArr[i]>=65 && passwordArr[i]<=90){cekzz[1]=1;cekzz[3]++;}
	        if(passwordArr[i]>=97 && passwordArr[i]<=122){cekzz[2]=1;cekzz[3]++;}
	    }
	    if(cekzz[0] && cekzz[1] && cekzz[2] && cekzz[3]>=6)isValidRe = 1; // password angka lowercase uppercase
	    if(isValid)break;
    }
    if(checkEmpty==0){//user pertama
	int cekzz[4]={0};
    	for(int i=0;i<strlen(passwordArr);i++){
	    if(passwordArr[i]>=48 && passwordArr[i]<=57)cekzz[0]=1;
	    if(passwordArr[i]>=65 && passwordArr[i]<=90){cekzz[1]=1;cekzz[3]++;}
	    if(passwordArr[i]>=97 && passwordArr[i]<=122){cekzz[2]=1;cekzz[3]++;}
	}
	if(cekzz[0] && cekzz[1] && cekzz[2] && cekzz[3]>=6)isValidRe = 1; // password angka lowercase uppercase
    }
    fclose(fp);   
}
```
Pada bagian soal ini diminta membuat fitur login/register. Pertama-tama ketika menjalankan program akan dibuat file `users.txt` pada server yang digunakan menyimpan
username dan password. Selanjutnya, fitur login/register dibuat dengan melakukan `strcmp()` pada input dari client. Jika memilih register maka client akan memasukkan
username dan password yang diinginkan. Data dianggap valid jika memenuhi kriteria:
- tidak ada username yang sama (diatasi dengan mengecek isi `users.txt`)
- password terdiri dari 6 huruf, lowercase, uppercase, dan angka (diatasi dengan mengecek tiap ASCII dari input password client)
Server akan menolak register sampai username dan password memenuhi syarat, lalu jika sudah akan meng-append string `username:password\n` pada `users.txt`.<br>
Jika memilih login maka client akan memasukkan username dan password, lalu jika ditemukan username dan password yang sesuai pada `users.txt` maka client berhasil login.
Selain itu, data nama user setelah berhasil login/register juga akan tersimpan pada array. Data nama ini akan digunakan pada beberapa poin berikutnya.
## 2B.
```c
void createTsv(){
    strcpy(tsvPath,dirPath);
    strcat(tsvPath,"/files.tsv");
    FILE *fp = fopen(tsvPath, "a+");
    fclose(fp);
}
```
Pada saat menjalankan server akan dibuat file berextension `.tsv` yang digunakan menyimpan judul dan author problem dipisahkan `\t`. Author problem bisa didapat dari
array nama yang tersimpan dari 1A dan formatting `\t` akan dilakukan ketika user melakukan add problem, karena pada kondisi awal file masih kosong.
## 2C.
### Server-side
```c
else if(strcmp(cmd,"add")==0){
    strcpy(judulProblem,"");
    strcpy(problemPath,"");
    isValidDir = 0;
    changeInput("Judul problem:");
    sendInput(new_socket);
    readBufferSp(new_socket);
    strcpy(judulProblem,cmd);
    strcpy(txtPath,dirPath);
    strcat(txtPath,"/");
    strcat(txtPath,judulProblem);
    checkDir();
    while(!isValidDir){
        changeInput("Directory exists, ganti judul problem:");
        sendInput(new_socket);
        readBufferSp(new_socket);
        strcpy(judulProblem,cmd);
        strcpy(txtPath,dirPath);
        strcat(txtPath,"/");
        strcat(txtPath,judulProblem);
        checkDir();
    }
    addData();

    strcat(out, "\nFilepath description.txt: ");
    strcat(out, judulProblem);
    strcat(out, "/description.txt");
    changeInput(out);
    sendInput(new_socket);
    saveTo(1);

    strcpy(out, "\nFilepath input.txt: ");
    strcat(out, judulProblem);
    strcat(out, "/input.txt");
    changeInput(out);
    sendInput(new_socket);
    saveTo(2);
    
    strcpy(out, "\nFilepath output.txt: ");
    strcat(out, judulProblem);
    strcat(out, "/output.txt");
    changeInput(out);
    sendInput(new_socket);
    saveTo(3);
    
    changeInput("\nData berhasil ditambahkan!");
    sendInput(new_socket);
}
```
### Client-side
```c
else if(strcmp(clientIn,"add")==0){
    //masukkan judul
    readBuffer();
    scanf("%[^\n]%*c", clientIn);
    changeInput(clientIn);
    sendInput();
    readBuffer();
    while(strcmp(cmd,"Directory exists, ganti judul problem:")==0){
        scanf("%[^\n]%*c", clientIn);
        changeInput(clientIn);
        sendInput();
        readBufferSp();
        printf("Server: %s\n",cmd);
    }
    readBuffer();
}
```
### CheckDir(), addData(), dan saveTo()
```c
void checkDir(){
    DIR* dir = opendir(txtPath);
    if (dir) {
        closedir(dir);
	isValidDir = 0;
    }
    else if (ENOENT == errno)isValidDir = 1;
    else isValidDir = 0;
}
void addData(){
	strcpy(txtPath,dirPath);
	strcat(txtPath,"/");
    strcat(txtPath,judulProblem);
    mkdir(txtPath, 0777);
	strcpy(txtPath,dirPath);
    strcat(txtPath,"/files.tsv");
    FILE * fp;
    fp = fopen (txtPath, "a+");
	printf("Judul problem: %s\n", judulProblem);
    fprintf(fp,"%s\t%s\n",judulProblem,usernameArr);
    fclose(fp);
    strcpy(problemPath,judulProblem);
}
void saveTo(int order){
    if(order==1){// description.txt
		char tempPath[1024];
		strcpy(tempPath,dirPath);
		strcat(tempPath,"/");
		strcat(tempPath,judulProblem);
		strcat(tempPath,"/description.txt");
		printf("Filepath description.txt: %s/description.txt\n", problemPath);

		FILE *fp = fopen(tempPath, "w");
		fclose(fp);
    }
    else if(order==2){// input.txt
		char tempPath[1024];
		strcpy(tempPath,dirPath);
		strcat(tempPath,"/");
		strcat(tempPath,judulProblem);
		strcat(tempPath,"/input.txt");
		printf("Filepath input.txt: %s/input.txt\n", problemPath);

		FILE *fp = fopen(tempPath, "w");
		fclose(fp);
    }
    else{// output.txt
		char tempPath[1024];
		strcpy(tempPath,dirPath);
		strcat(tempPath,"/");
		strcat(tempPath,judulProblem);
		strcat(tempPath,"/output.txt");
		printf("Filepath output.txt: %s/output.txt\n", problemPath);

		FILE *fp = fopen(tempPath, "w");
		fclose(fp);
    }
}
```
Setelah berhasil login/register, maka akan dilakukan infinite loop untuk menerima perintah dari client sampai client menggunakan perintah `logout`, salah satunya
adalah `add`. Ketika menjalankan perintah `add`, client akan diminta memasukkan judul problem dimana ketika directory dengan nama judul problem tersebut sudah
ada pada server maka client diminta memilih judul problem lain. Lalu client akan diminta memasukkan path description.txt, input.txt, dan output.txt.<br><br>
Agar dapat mengirim ke server, salah satu cara yang dapat dilakukan adalah dengan membuka isi file pada sisi client lalu menyimpan isi file pada buffer dan mengirim
ke server. Selanjutnya server akan menyimpan isi buffer pada directory judul problem sesuai ketentuan. Selain itu server juga akan mengupdate file `.tsv` dengan format
`<judul>\t<author>`
## 2D.
### Server-side
```c
if(strcmp(cmd,"see")==0){
    seeTsv(new_socket);
}
```
### Client-side
```c
if(strcmp(clientIn,"see")==0){
    readBufferSp();
    printf(".tsv list:\n%s",cmd);
}
```
### seeTsv()
```c
void seeTsv(int new_socket){
    strcpy(input,"");
	strcpy(txtPath,dirPath);
    strcat(txtPath,"/files.tsv");
    FILE * fp;
    fp = fopen (txtPath, "r");
    char left[100],right[100];
    while (fscanf(fp,"%s\t%s\n",left,right) != EOF){
	strcat(input,left);
	strcat(input," by ");
	strcat(input,right);
	strcat(input,"\n");
    }
    fclose(fp);
    if(strcmp(input,"")==0)strcpy(input,"No problem yet\n");
    send(new_socket , input , strlen(input) , 0 );
}
```
Client juga dapat melakukan perintah `see` yang menampilkan isi dari file `.tsv` dengan format `<judul> by <author>`. Caranya dengan server membuka file `.tsv` lalu
mengganti delimiter `\t` dengan ` by ` lalu mengirim isinya ke client. Dalam hal ini agar terlihat rapi maka tampilan pada client akan memiliki format:
`Server: .tsv list` diikuti baris berikutnya yaitu `<judul> by <author>`. Jika file `.tsv` kosong maka server juga akan mengirim pesan yang menunjukkan bahwa file
`.tsv` masih kosong.
## 2E.
### Server-side
```c
if(strcmp(cmd,"download")==0){
    isValidDir = 0;
    changeInput("Judul problem:");
    sendInput(new_socket);
    readBufferSp(new_socket);
    strcpy(judulProblem,cmd);
    strcpy(txtPath,dirPath);
    strcat(txtPath,"/");
    strcat(txtPath,judulProblem);
    checkDir();
    while(isValidDir){
        changeInput("Directory doesn't exist, ganti judul problem:");
        sendInput(new_socket);
        readBufferSp(new_socket);
        strcpy(judulProblem,cmd);
        checkDir();
    }
    download(1);
    download(2);
    changeInput("\nDownload berhasil!");
    sendInput(new_socket);
}
```
### Client-side
```c
if(strcmp(clientIn,"download")==0){
    readBuffer();
    scanf("%[^\n]%*c", clientIn);
    changeInput(clientIn);
    sendInput();
    readBuffer();
    while(strcmp(cmd,"Directory doesn't exist, ganti judul problem:")==0){
        scanf("%[^\n]%*c", clientIn);
        changeInput(clientIn);
        sendInput();
        readBufferSp();
        printf("Server: %s\n",cmd);
    }
    readBuffer();
}
```
### download()
```c
void download(int order){
	char sourcePath[1024], targetPath[1024];
	strcpy(sourcePath,dirPath);
	strcat(sourcePath,"/");
    strcat(sourcePath,judulProblem);

    strcpy(targetPath,"/home/ubuntu/Desktop/sisop/modul3/soal-shift-sisop-modul-3-ita04-2022/soal2/Client");
	strcat(targetPath,"/");
    strcat(targetPath,judulProblem);
    mkdir(targetPath, 0777);

	if(order==1){// description.txt
		strcat(sourcePath,"/description.txt");
		strcat(targetPath,"/description.txt");
		downloadSoal(sourcePath, targetPath);
	}
	else{// input.txt
		strcat(sourcePath,"/input.txt");
		strcat(targetPath,"/input.txt");
		downloadSoal(sourcePath, targetPath);
	}
}
```
### downloadSoal()
```c
void downloadSoal(char *sourcePath, char*targetPath){
    char ch;
	FILE *source, *target;
	source = fopen(sourcePath, "r");
	if (source == NULL) {
		exit(EXIT_FAILURE);
	}
	target = fopen(targetPath, "w");
	if (target == NULL) {
		fclose(source);
		exit(EXIT_FAILURE);
	}
	while ((ch = fgetc(source)) != EOF)
		fputc(ch, target);
	printf("File copied successfully.\n");
	fclose(source);
	fclose(target);
}
```
Selanjutnya, terdapat fitur `download`. Fitur ini terdiri dari 2 kata yaitu download dan <nama problem>. Untuk membuat fitur ini maka Server akan mengirim path
dari directory nama problem yang diinginkan, lalu dari client akan mengambil isi `input.txt` dan `description.txt` dari problem tersebut dan membuat directory
dengan nama yang sama pada client-side.
## 2F.
### Server-side
```c
else if(strcmp(cmd,"submit")==0){
    char out_client[100];
    changeInput("Judul problem:");
    sendInput(new_socket);
    readBufferSp(new_socket);
    strcpy(judulProblem,cmd);
    strcpy(txtPath,dirPath);
    strcat(txtPath,"/");
    strcat(txtPath,judulProblem);

    changeInput("File output:");
    sendInput(new_socket);
    readBufferSp(new_socket);
    strcpy(out_client,cmd);
    strcpy(answerPath,"/home/ubuntu/Desktop/sisop/modul3/soal-shift-sisop-modul-3-ita04-2022/soal2/Client");
    strcat(answerPath,"/");
    strcat(answerPath,judulProblem);
    strcat(answerPath,"/");
    strcat(answerPath,out_client);
    submit();
    sendInput(new_socket);
}
```
### Client-side
```c
else if(strcmp(clientIn,"submit")==0){
    readBuffer();
    scanf("%[^\n]%*c", clientIn);
    changeInput(clientIn);
    sendInput();
    readBuffer();//AC or WA
}
```
### submit()
```c
void submit(){
    FILE *ans, *corr;
	int check = 0;
    char correct[1024], tested[1024];
    strcat(txtPath,"/output.txt");
	ans = fopen (txtPath, "r");
	corr = fopen (answerPath, "r");
	while(fgets(correct, 1024, corr) != NULL){
		if(fgets(tested, 1024, ans) != NULL){
			if(strcmp(correct, tested)!=0){
				changeInput("WA");
				check = 0;
				break;
			}
			else check = 1;
		}
	}
	if(check == 1) changeInput("AC");
	fclose(ans);
	fclose(corr);
}
```
Terdapat juga fitur `submit` yang terdiri dari 3 kata submit, <nama problem>, dan path hasil output client. Cara menyelesaikan poin ini adalah dengan server
menerima nama problem dan path dari hasil output client, lalu membandingkan hasil output tersebut dengan `output.txt` pada nama problem yang tersimpan pada server-side
yang berfungsi seperti kunci jawaban. Jika hasil berbeda maka server akan mengembalikan pesan `WA` dan ketika sama maka server akan mengembalikan pesan `AC`.
## 2G.
### Server-side
```c
void *client(void *tmp){
    int new_socket = *(int *)tmp;
    printf("A client connected\n");
    if (ctrClient == 1) {
	changeInput("Register / Login? (case sensitive)");
	sendInput(new_socket);
    }
    else {
	changeInput("Wait other client to close connection...");
	sendInput(new_socket);
    }
    
    while (ctrClient > 1) {
        readBuffer(new_socket);
        if (ctrClient == 1) {
            changeInput("Register / Login? (case sensitive)");
	    sendInput(new_socket);
        }
        else {
            changeInput("Wait other client to close connection...");
	    sendInput(new_socket);
        }
}

int main(int argc, char const *argv[]) {
    int new_socket;
    strcpy(cmd,"");
    createTxt();
    createTsv();

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
      
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );
      
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }
    while(1){
	if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
	    perror("accept");
	    exit(EXIT_FAILURE);
	}
	pthread_create(&(tid[ctrClient]), NULL, &client, &new_socket);
        ctrClient++;
    }
    
    return 0;
}
```
### Client-side
```c
int main(int argc, char const *argv[]) {
    strcpy(cmd,"");
    
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }
  
    memset(&serv_addr, '0', sizeof(serv_addr));
  
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
      
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
  
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }

    strcpy(cmd, "");
    printf("Connected to Server\n");
    readBufferSp();
    printf("Server: %s\n",cmd );
    while (strcmp(cmd, "Register / Login? (case sensitive)")!=0) {
        printf("Refresh connection with random chat\n");
        scanf("%[^\n]%*c", clientIn);
        changeInput(clientIn);
        sendInput();
        readBufferSp();
        printf("Server: %s\n",cmd );
    }
}
```
### logout (Server-side)
```c
else if(strcmp(cmd,"logout")==0){
    printf("A client closed connection");
    changeInput("Successfully disconnected");
    sendInput(new_socket);
    close(new_socket);
        ctrClient--;
    break;
}
```
### logout (Client-side)
```c
else if(strcmp(clientIn,"logout")==0){
    readBuffer();
    break;
}
```
Untuk menyelesaikan poin ini, perlu menggunakan thread sehingga tiap client akan berjalan pada thread yang berbeda. Selain itu diperlukan juga `ctrClient` yang menyimpan jumlah client yang terhubung. Hal ini bertujuan agar ketika jumlah client lebih dari 1, maka client yang lain akan terjebak dalam loop sampai client pertama melakukan logout, logout dalam hal ini juga akan otomatis mengurangi nilai `ctrClient`. Nilai `new_socket` pada server akan berbeda untuk tiap client, tujuannya agar pesan tidak tercampur.<br>

## Screenshot

![gambar 1](https://gitlab.com/fernandosibarani45/screenshot/-/raw/master/soal-shift-sisop-modul-3-ita04-2022/soal%202/register.jpg)
![gambar 2](https://gitlab.com/fernandosibarani45/screenshot/-/raw/master/soal-shift-sisop-modul-3-ita04-2022/soal%202/login.jpg)
![gambar 3](https://gitlab.com/fernandosibarani45/screenshot/-/raw/master/soal-shift-sisop-modul-3-ita04-2022/soal%2032/add.jpg)
![gambar 4](https://gitlab.com/fernandosibarani45/screenshot/-/raw/master/soal-shift-sisop-modul-3-ita04-2022/soal%202/see.jpg)
![gambar 5](https://gitlab.com/fernandosibarani45/screenshot/-/raw/master/soal-shift-sisop-modul-3-ita04-2022/soal%202/download.jpg)
![gambar 6](https://gitlab.com/fernandosibarani45/screenshot/-/raw/master/soal-shift-sisop-modul-3-ita04-2022/soal%202/submitAC.jpg)
![gambar 7](https://gitlab.com/fernandosibarani45/screenshot/-/raw/master/soal-shift-sisop-modul-3-ita04-2022/soal%202/submitWA.jpg)
![gambar 8](https://gitlab.com/fernandosibarani45/screenshot/-/raw/master/soal-shift-sisop-modul-3-ita04-2022/soal%202/multi1.jpg)
![gambar 8](https://gitlab.com/fernandosibarani45/screenshot/-/raw/master/soal-shift-sisop-modul-3-ita04-2022/soal%202/multi2.jpg)

# Soal No. 3

## Cara Pengerjaan
Untuk nomor 3, terdapat 3 file yaitu soal3.c, client.c, dan server.c

Untuk soal3.c, program ini akan mengkategorikan folder yang ada di hartakarun sesuai dengan ekstensi yang dimiliki. File client berfungsi untuk zip folder hasil pengkategorian. Sedangkan program server.c berfungsi untuk melakukan koneksi dengan file client.c.

```c
int main()
{
    pthread_mutex_init(&process_mutex, NULL);

    chdir("hartakarun");
    tranversalDirectory(".");
    joinThread();

    pthread_mutex_destroy(&process_mutex);

    return 0;
}
```

Cara kerja dari file soal nomor 3 adalah dengan melakukan transvers direktori secara rekursive.

```c
void tranversalDirectory(char *basePath)
{
    char path[1000];
    struct dirent *dp;
    DIR *dir = opendir(basePath);
    DIR* check;
    if (!dir) return;

    while ((dp = readdir(dir)) != NULL)
    {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
        {
            strcpy(path, basePath);
            strcat(path, "/");
            strcat(path, dp->d_name);

            check = opendir(path);
            if(!check){
                char** argv = (char**)malloc(2 * sizeof(char*));
                argv[0] = (char*)malloc(2000 * sizeof(char));
                argv[1] = (char*)malloc(1000 * sizeof(char));

                strcpy(argv[0], dp->d_name);
                strcpy(argv[1], path);
                
                pthread_create(&(thread[++it]), NULL, processFile, argv);
            }
            tranversalDirectory(path);
        }
    }
    closedir(dir);
}
```

Ketika suatu file dijumpai ketika melakukan tranvers, maka akan diinisiasi thread untuk mengolah file tersebut.

```c
void *processFile(void *argv){
    pthread_mutex_lock(&process_mutex);
    char **temp = (char**) argv;
    char *ext = getExt(temp[0]);

    createDirectory(ext);
    moveFile(temp[0], temp[1], ext);

    free(argv);
    pthread_mutex_unlock(&process_mutex);
}
```

Cara program dalam memproses adalah dengan membuat folder dengan nama ekstensinya terlebih dahulu, kemudian memindahkan file tersebut ke folder yang dibuat tadi.

```c
void createDirectory(char *dir_Name){
    mkdir(dir_Name,0777);
}
```

Untuk membuat filenya, kami menggunakan fungsi mkdir dengan akses 777. Ketika folder dengan nama yang sama, maka fungsi ini tidak akan mengosongkan isi file yang ada dan tidak membuat folder baru dengan nama lain sehingga tidak perlu dilakukan pengecekan apakah folder yang akan dibuat sudah ada atau belum.

```c
void moveFile(char *namaFile, char *origin, char *destination){
    char dest[2000];
    
    snprintf(dest, sizeof(dest), "./%s/%s", destination, namaFile);
    printf("%d %s\n", rename(origin, dest), origin);
}
```

Kemudian untuk memindahkan filenya kami memanfaatkan fungsi rename dengam menambahkan nama direktori tujuan didepan nama filenya. Sehingga file akan berpindah ke direktori yang diinginkan.

Dapat dilihat di fungsi main, ketika transvers direktori telah selesai dilakukan maka akan dilakukan fungsi join thread.

```c
void joinThread(){
    for(; it>=0; it--){
        pthread_join( thread[it], NULL);
    }
}
```

Fungsi ini berguna untuk melakukan join thread yang telah diinisiasi tadi. Kemudian mutex yang kami gunakan di setiap fungsi yang dijalankan thread tadi ditujukan untuk menghindari adanya race condition.

Setelah isi folder dari hargakarun sudah dikategorikan dengan program soal3.c, maka kemudian akan server dan client.

Untuk fungsi mainnya kami menggunakan template client dari modul sebelumnya. Namun kami menambahkan perintah execv untuk melakukan zip hasil folder yang sudah dikategorikan tadi. Setelah itu kami menambahkan fungsi send file untuk melakukan pengiriman file zip hasil kategorisasi yang tadi.

```c
void send_file(int socket, char *fname)
{
    char buffer[MAX_LENGTH] = {0};
    char fpath[MAX_LENGTH];

    strcpy(fpath, "./soal3/Client/");
    strcat(fpath, fname);
    FILE *file = fopen(fpath, "r");
    
    if (file == NULL)
    {
        printf("File %s not found.\n", fname);
        return;
    }

    memset(buffer, 0, MAX_LENGTH);

    int file_size;

    while (file_size > 0)
    {   
        file_size = fread(buffer, sizeof(char), MAX_LENGTH, file);
        if (send(socket, buffer, file_size, 0) < 0)
        {
            fprintf(stderr, "Failed to send file %s\n", fname);
            return;
        }
        memset(buffer, 0, MAX_LENGTH);
    }
    fclose(file);
    printf("Kirim File Berhasil\n");
}
```

Cara kerja fungsi ini adalah dengan membaca apakah file zipnya sudah ada atau belum. Jika sudah ada, maka akan dilakukan pengiriman file dengan menggunakan fungsi send(). Kemudian direktori yang dibuka ditutup kembali.

Untuk server juga menggunakan template server dari modul sebelumnya. Namun kami menambahkan fungsi untuk menerima file yang diterima.

```c
void receive_file(int socket, char *fname)
{
    char buffer[MAX_LENGTH] = {0};
    char fpath[MAX_LENGTH];
    
    strcpy(fpath, "./soal3/Server/");
    strcat(fpath, fname);
    FILE *file_masuk = fopen(fpath, "wb");

    if (file_masuk == NULL) printf("File %s, can't be made.\n", fname);
    else
    {
        memset(buffer, 0, MAX_LENGTH);
        int file_size = 0;
        
        while ((file_size = recv(socket, buffer, MAX_LENGTH, 0)) > 0)
        {
            int write_size = fwrite(buffer, sizeof(char), file_size, file_masuk);
            
            if (write_size < file_size){
                perror("Failed to write file.");
                return;
            }
            memset(buffer, 0, MAX_LENGTH);
            if (file_size == 0 || file_size != MAX_LENGTH) break;
        }
        
        if (file_size < 0)
        {
            fprintf(stderr, "Downloading file failed\n");
            exit(1);
        }
    }
  fclose(file_masuk);
  printf("Terima File Berhasil\n");
}
```

Fungsi tersebut bekerja dengan cara pertama-tama membuka direktori tempat tujuan file hasil transfer diterima. Kemudian akan dilakukan penerimaan file dengan menggunakan fungsi recv(). Kemudian akan dilakukan beberapa pengecekan dari fungsi recv yang telah dilakukan. Setelah melewati pengecekan-pengecekan tersebut, berarti file yang diterima sudah berhasil dan file hasil zip sudah berada di folder server.

## Screenshot

![gambar 1](https://gitlab.com/fernandosibarani45/screenshot/-/raw/master/soal-shift-sisop-modul-3-ita04-2022/soal%203/messageImage_1650207440732.jpg)
![gambar 2](https://gitlab.com/fernandosibarani45/screenshot/-/raw/master/soal-shift-sisop-modul-3-ita04-2022/soal%203/messageImage_1650207501414.jpg)
![gambar 3](https://gitlab.com/fernandosibarani45/screenshot/-/raw/master/soal-shift-sisop-modul-3-ita04-2022/soal%203/messageImage_1650207872727.jpg)
![gambar 4](https://gitlab.com/fernandosibarani45/screenshot/-/raw/master/soal-shift-sisop-modul-3-ita04-2022/soal%203/messageImage_1650208102238.jpg)

## Kendala yang Dihadapi
Terdapat kendala pada saat melakukan pengiriman file sehingga file tidak dikirim seutuhnya.
