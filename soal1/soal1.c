#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <pthread.h>
#include <dirent.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdint.h>

pthread_t thread[2];
pthread_mutex_t process_mutex;
int it=-1;
static char *decoding_table = NULL;
FILE *fp, *fptr;
char buffer[4096];
static char encoding_table[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                                'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                                'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                                'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                                'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                                'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                                'w', 'x', 'y', 'z', '0', '1', '2', '3',
                                '4', '5', '6', '7', '8', '9', '+', '/'};

void joinThread();
void jalankan(char *perintah, char*argv[]);
void* createDirectory(void *argv);
void* ekstrak(void *argv);
void* processFile(void *argv);
void build_decoding_table();
unsigned char *base64_decode(const char *data);
void tranversalDirectory(char *basePath);
void* permintaanKhusus1();
void* permintaanKhusus2();

int main()
{
    char *argv1, *argv2;
    pthread_mutex_init(&process_mutex, NULL);

    argv1 = "./soal1/music"; argv2 = "./soal1/quote";
    pthread_create(&(thread[++it]), NULL, createDirectory, argv1);
    pthread_create(&(thread[++it]), NULL, createDirectory, argv2);
    joinThread();

    char *arrgv1[] = {"music.zip", "./soal1/music"};
    char *arrgv2[] = {"quote.zip", "./soal1/quote"};
    pthread_create(&(thread[++it]), NULL, ekstrak, arrgv1);
    pthread_create(&(thread[++it]), NULL, ekstrak, arrgv2);
    joinThread();

    char *arrgv3[] = {"./soal1/quote", "./soal1/quote/quote.txt"};
    char *arrgv4[] = {"./soal1/music", "./soal1/music/music.txt"};
    pthread_create(&(thread[++it]), NULL, processFile, arrgv3);
    pthread_create(&(thread[++it]), NULL, processFile, arrgv4);
    joinThread();

    char *argv3 = "./soal1/hasil";
    createDirectory(argv3);

    char *arrgv5[] = {"mv", "./soal1/quote/quote.txt", "./soal1/hasil/quote.txt", NULL};
    char *arrgv6[] = {"mv", "./soal1/music/music.txt", "./soal1/hasil/music.txt", NULL};
    jalankan("/usr/bin/mv", arrgv5);
    jalankan("/usr/bin/mv", arrgv6);

    char *arrgv7[] = {"zip", "--password", "mihinomenestubuntu", "-r", "./soal1/hasil.zip", "-j", "./soal1/hasil", NULL};
    jalankan("/bin/zip", arrgv7);

    pthread_create(&(thread[++it]), NULL, permintaanKhusus1, NULL);
    pthread_create(&(thread[++it]), NULL, permintaanKhusus2, NULL);

    char *arrgv8[] = {"zip", "--password", "mihinomenestubuntu", "-r", "./soal1/hasil.zip", "-j", "./soal1/hasil", NULL};
    jalankan("/bin/zip", arrgv8);

    pthread_mutex_destroy(&process_mutex);
    return 0;
}

void* permintaanKhusus1(){
    char *argv7[] = {"unzip", "--password", "mihinomenestubuntu", "-r", "./soal1/hasil.zip", "-j", "./soal1/hasil", NULL};
    jalankan("/bin/unzip", argv7);
}

void* permintaanKhusus2(){
    FILE *file;
    file = fopen("./soal1/hasil/no.txt", "w");
    fputs("No", file);
    fclose(file);
}

void tranversalDirectory(char *basePath)
{
    char path[1000];
    struct dirent *dp;
    DIR *dir = opendir(basePath);
    DIR* check;
    if (!dir) return;

    while ((dp = readdir(dir)) != NULL)
    {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
        {
            strcpy(path, basePath);
            strcat(path, "/");
            strcat(path, dp->d_name);

            check = opendir(path);
            if(!check){
                fp = fopen(path, "r");
                fread(buffer, 4096, 1, fp);

                fputs(base64_decode(buffer), fptr);
                fputs("\n", fptr);
                
                fclose(fp);
            }
            tranversalDirectory(path);
        }
    }
    closedir(dir);
}

unsigned char *base64_decode(const char *data) {

    size_t input_length = strlen(data);
    size_t *output_length;

    if (decoding_table == NULL) build_decoding_table();

    if (input_length % 4 != 0) return NULL;

    *output_length = input_length / 4 * 3;
    if (data[input_length - 1] == '=') (*output_length)--;
    if (data[input_length - 2] == '=') (*output_length)--;

    unsigned char *decoded_data = malloc(*output_length);
    if (decoded_data == NULL) return NULL;

    for (int i = 0, j = 0; i < input_length;) {

        uint32_t sextet_a = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_b = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_c = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_d = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];

        uint32_t triple = (sextet_a << 3 * 6)
        + (sextet_b << 2 * 6)
        + (sextet_c << 1 * 6)
        + (sextet_d << 0 * 6);

        if (j < *output_length) decoded_data[j++] = (triple >> 2 * 8) & 0xFF;
        if (j < *output_length) decoded_data[j++] = (triple >> 1 * 8) & 0xFF;
        if (j < *output_length) decoded_data[j++] = (triple >> 0 * 8) & 0xFF;
    }

    return decoded_data;
}

void build_decoding_table() {

    decoding_table = malloc(256);

    for (int i = 0; i < 64; i++)
        decoding_table[(unsigned char) encoding_table[i]] = i;
}

void* processFile(void *argv){
    pthread_mutex_lock(&process_mutex);

    char **file = (char**) argv;

    fptr = fopen(file[1], "a");
    tranversalDirectory(file[0]);
    fclose(fptr);
    pthread_mutex_unlock(&process_mutex);
}

void* ekstrak(void *argv){
    pthread_mutex_lock(&process_mutex);

    char **data = (char**) argv;
    
    char *arg[] = {"unzip", "-j", data[0], "-d", data[1], NULL};
    jalankan("/bin/unzip", arg);

    pthread_mutex_unlock(&process_mutex);
}

void* createDirectory(void *argv){
    pthread_mutex_lock(&process_mutex);

    char* dir = (char*) argv;
    
    char *arg[] = {"mkdir", "-p", dir, NULL};
    jalankan("/bin/mkdir", arg);

    pthread_mutex_unlock(&process_mutex);
}

void jalankan(char *perintah, char*argv[]){
    int status=0;
    if(fork()==0)
        execv(perintah, argv);
    while(wait(&status)>0);
}

void joinThread(){
    for(; it>=0; it--){
        pthread_join( thread[it], NULL);
    }
}